package com.javafundamentals.store;

import com.javafundamentals.store.common.controller.StartController;
import com.javafundamentals.store.common.util.LoggerUtil;
import com.javafundamentals.store.exception.LoggerException;
import com.javafundamentals.store.exception.UserIsNotSignInException;

public class StoreApplication {

	public static void main(String[] args) throws UserIsNotSignInException, LoggerException {
		var logger = new LoggerUtil();
		logger.initLogger();
		var startController = new StartController();
		startController.start();
	}

}
